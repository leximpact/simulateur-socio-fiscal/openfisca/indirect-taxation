# BDF - instruction de collecte - VF

Protocole de collecte : un calendrier en 6 vagues distinctes

Il est important de considérer les 6 vagues comme totalement indépendantes et d'en effectuer la collecte dans la période indiquée.

## Echantillon

Metropole:
- L'échantillon est contitué de 20 700 FA, soit environ 3 400 par vague.
- s'ajoute un échantillon complémentaire composé de familles monoparentales de 2 000 FA.

Aux Antilles, en Guyane et à La Réunion:
- Il comprend 1 660 FA par DOM, soit environ 280 FA par vague et par DOM.

sous-échantillons:
- ménage composé d’une seule personne
- ménage composé de 2 ou 3 personnes
- ménage composé de 4 personnes ou plus
- famille monoparentale (uniquement métropole)

code SSECH est composé de 2 chiffres:
- le 1er indique le numéro de la vague,
- le 2nd indique le numéro de la strate.

Par exemple, lorsque SSECH = 32, cela signifie que la FA appartient à la 3e vague de l’enquête et à la strate des ménages composés de 2 ou 3 personnes au moment du recensement.

## Deroulement de l'enquête

Le questionnement se déroule en **deux visites espacées de 8 jours au minimum.** La seconde visite doit, quant à elle, se dérouler dans des délais raisonnables c’est-à-dire au plus 15 jours après la première visite.

## Questionnaires

Il est important de bien noter que le vouvoiement ne s’adresse pas ici à la personne répondante (ses propres achats) mais à l’ensemble du ménage (« vous » pluriel).

ANNEXE 1 Récapitulation sur les périodes de référence:

| Période de référence | Module | Nom du sous module |
| :--------------- |:---------------:| :-----|
| 12 mois | Module 1.3 | Électroménager |
| 12 mois | Module 1.3 | Audiovisuel et musique |
| 12 mois | Module 1.3 | Informatique et mobile |
| 12 mois | Module 1.3 | Équipement numérique de loisirs |
| 12 mois | Module 1.3 | Autres appareils et biens de loisirs |
| 12 mois | Module 1.3 | Jardinage et bricolage |
| 12 mois | Module 1.3 | Compléments à l’étude des biens durables |
| 12 mois | Module 1.5 | Séjour hors du domicile |
| 12 mois | Module 1.6 | Frais scolaires et universitaires |
| 12 mois | Module 1.6 | Dépenses liées aux animaux de compagnie |
| 12 mois | Module 1.7 | Audiovisuel |
| 12 mois | Module 1.7 | Culture et loisirs |
| 12 mois | Module 1.7 | Soins du corps |
| 12 mois | Module 1.8 | Hopital |
| 12 mois | Module 1.8 | Achat ou location d’appareils médicaux |
| 12 mois | Module 1.8 | Cotisation à une mutuelle |
| 12 mois | Module 1.9 | Pensions, aides, et dons REGULIERS en argent |
| 12 mois | Module 1.10 |  Pensions, aides, et dons reçus régulièrement en argent |
| 12 mois | Module 1.10 | Aides et Cadeaux reçus en nature |
| 12 mois | Module 2.1 | Propriétaires et accédants |
| 12 mois | Module 2.1 | Locataires et sous-locataires |
| 12 mois | Module 2.1 | Charges |
| 12 mois | Module 2.1 | Eau, électricité, gaz |
| 12 mois | Module 2.2 | Résidences secondaires et autres logements |
| 12 mois | Module 2.3 | Terrains |
| 12 mois | Module 2.4 | Compléments à l’étude des logements et des terrains |
| 12 mois | Module 2.5 | Automobiles |
| 12 mois | Module 2.5 | Vélos |
| 12 mois | Module 2.5 | Deux roues |
| 12 mois | Module 2.5 | Autres véhicules de transport |
| 12 mois | Module 2.5 | Compléments à l’étude des véhicules de transport |
| 12 mois | Module 2.5 | Frais relatifs aux transports, hors véhicules personnels |
| 12 mois | Module 2.6 | Objets d’art de valeur |
| 12 mois | Module 2.8 | Assurances, retraites complémentaires |
| 12 mois | Module 2.8 | Crédits à rembourser |
| 12 mois | Module 2.8 | Impôts sur le revenu |
| 12 mois | Module 2.8 | Cartes de crédit et autres services financiers |
| 12 mois | Module 2.8 | Dépenses exceptionnelles |
| 12 mois | Module 2.9 | Les revenus que nous ne pourrons pas trouver dans la déclaration d’impôt |
| 12 mois | Module 2.9 | Salaires, prestations chômage, RMI, RSA |
| 12 mois | Module 2.9 | Pensions |
| 12 mois | Module 2.9 | Allocations familiales et aides aux études |
| 12 mois | Module 2.10 | Revenus de l’épargne |
| 12 mois | Module 2.11 | Ressources exceptionnelles |
| 2 mois | Module 1.4 | Vêtements et chaussures |
| 2 mois | Module 1.6 | Suivi et soutien scolaire et universitaire |
| 2 mois | Module 1.6 | Garde d’enfants hors du domicile |
| 2 mois | Module 1.6 | Services domestiques à domicile |
| 2 mois | Module 1.8 | Généraliste |
| 2 mois | Module 1.8 | Spécialiste |
| 2 mois | Module 1.8 | Pharmacie |
| 2 mois | Module 1.8 | Laboratoire |
| 2 mois | Module 1.9 | Aides et Cadeaux offerts occasionnellement en argent |
| 2 mois | Module 1.9 | Aides et Cadeaux en nature offerts |
| 2 mois | Module 1.10 | Aides et Cadeaux reçus occasionnellement en argent |
| 2 mois | Module 2.7 | Frais relatifs aux repas payants pris sur le lieu scolaire ou de travail |
| 2 mois | Module 2.8 | Dépenses occasionnées par une personne vivant hors du domicile au moins un jour par semaine |

# BDF 2016 - questionnaire 1

TRONC COMMUN DES MENAGES (TCM)

THL : Tableau des Habitants du Logement

- Bloc A. Liste et état-civil des habitants du logement
- Bloc B. Situation familiale
- Bloc C. Contour des ménages
- Bloc D. Lieux de vie
- Bloc E. Situation principale vis-à-vis du travail et groupe de référence

DUV : Description du ménage (unité de vie)

- Bloc F. Activité professionnelle
- Bloc G. Ressources culturelles

# bdf 2016 - questionnaire 2

MODULE 5 - TRANSPORTS INDIVIDUELS ET COLLECTIFS (P 36)

- Sous-module 1. Automobiles
- Sous-module 2. Vélos
- Sous-module 3. Deux-roues
- Sous-module 4. Autres véhicules de transport
- Sous-module 5. Compléments à l'étude des véhicules de transport
- Sous-module 6. Transports collectifs
- Sous-module 7. Déplacements domicile-lieu de travail

# Dictionnaire

## QUELQUES CARACTÉRISTIQUES DE L’ENQUÊTE BUDGET DE FAMILLE 2017

Les revenus manquants issus du questionnaire ont été imputés de deux manières différentes :
- les revenus importants et/ou comportant assez d’individus ont été imputés par la méthode des résidus simulés. Elle consiste à prédire les valeurs à imputer grâce à un modèle économétrique (régression ou modèle de durée si des montants en tranche sont disponibles) puis à imputer les valeurs manquantes par la somme de la prédiction et d’un résidu simulé (sous contrainte que le montant imputé se trouve dans la tranche éventuellement déclarée) ;
- les autres revenus ont été imputés par hot-deck aléatoire stratifié. Mais pour la plus grande partie, les revenus sont issus de rapprochement avec des sources fiscales et administratives. Ils ont fait l’objet de traitements spécifiques (cf. partie suivante).

## lexique

### Les logements sont répartis en quatre catégories :
- résidences principales
- résidences secondaires
- logements occasionnels
- logements vacants

### Ménage (au sens des enquêtes auprès des ménages) :

Depuis 2005, la définition d'un ménage, au sens des enquêtes auprès des ménages réalisées par l'Insee, a été sensiblement modifiée. Est considéré comme un ménage l'ensemble des personnes (apparentées ou non) qui partagent de manière habituelle un même logement (que celui-ci soit ou non leur résidence principale) et qui ont un budget en commun. La résidence habituelle est le logement dans lequel on a l'habitude de vivre.

**Font donc partie du même ménage des personnes qui ont un budget commun, c'est-à-dire :**
- qui apportent des ressources servant à des dépenses faites pour la vie du ménage
- et/ou qui bénéficient simplement de ces dépenses
Seuls les ménages ordinaires sont enquêtés, c’est-à-dire les ménages ne vivant pas en collectivité (maisons de retraites, casernes, cités universitaire, etc.).

**Remarques :**
- Dans la définition du budget commun, on ne tient pas compte des dépenses faites pour le logement.
- La participation occasionnelle à des dépenses communes ne suffit pas à former un budget commun.
- Avoir plusieurs comptes en banque différents dans un ménage ne signifie pas faire budget à part.

Dans les enquêtes réalisées avant 2005, les personnes devaient partager la même résidence principale pour être considérées comme des ménages (ou « ménages ordinaires »). Par ailleurs, il n'était pas nécessaire qu'elles aient un budget commun. De fait, un ménage correspondait à un logement (résidence principale). En revanche, depuis 2005, un logement peut comporter plusieurs ménages appelés encore « unités de vie ».

### Personne de référence du ménage (PR) :

La personne de référence du ménage est le principal apporteur de ressource. Lorsqu’il y a plusieurs principaux apporteurs de ressources, la personne de référence est en priorité l’actif, le retraité, puis l’autre inactif ; à statut égal, la personne de référence est la personne la plus âgée.

### Couple (au sens des enquêtes auprès des ménages) :

Un couple est composé de deux personnes de 15 ou plus, habitant le même logement et déclarant actuellement être en couple, quel que soit leur état matrimonial légal (qu'elles soient donc mariées ou non).

## INDEX THÉMATIQUE DES VARIABLES PAR TABLE

- table Automobile page 68
- Table Autvehic page 69

# Nomenclature

- 072*** Frais d'utilisation de véhicules personnels (entretien, essence, garagiste, parking, péage)
- 073*** Services de transport
- 074*** Autres dépenses de transport

# Tropicalisation Mayotte

=/= utile pour metropole

# Variables suprimées du FPR

## Les variables suivantes ont été supprimées du fichier après redressement:
- toutes les variables d’adresse, la région, le département et tous les identifiants géographiques infra départementaux,
- les prénoms,
- les jours de naissance,
- les libellés en clair des professions,
- le numéro d’enquêteur,
- les variables de gestion et de prise de contact.

## Par ailleurs, certaines variables ont été agrégées. Il s’agit de :

- la catégorie socioprofessionnelle (CS24 et CS42 à 2 chiffres),
- la nationalité en 7 modalités
- le pays de naissance en 6 modalités
- les dépenses, regroupées au ni veau 5 de la nomenclature COICOP (conformément à la règle de diffusion d’Eurostat)
- variables de revenus et de ressources monétaires :
Ces variables sont arrondies à la dizaine d’euros. Elles résultent d’informations déclarées par les foyers fiscaux mais aussi d’estimations complémentaires visant à corriger
la non réponse résultant des appariements entre l’enquête et la source fiscale ou à couvrir les revenus exonérés d’impôt ou soumis à prélèvement libératoire (livrets exonérés, assurance vie, revenus fin anciers issus de PEA, …). Plusieurs variables de revenus sont diffusées, au niveau individuel ou au niveau ménage selon les cas : notamment le revenu
total du ménage, les salaires et traitements, les revenus d’activité non salariée, les allocations chômage et préretraite, les pensions de retraite et les revenus du patrimoine.
- Les identifiants du ménage sont anonymisés par génération d’identifiants aléatoires.