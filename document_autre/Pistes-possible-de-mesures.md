# Pistes possible de mesures en étude.


[Prix du carburant : chèque inflation, aide ciblée, chèque carburant... Quelles sont les pistes ? - RTL](https://www.rtl.fr/actu/politique/prix-du-carburant-cheque-inflation-aide-ciblee-cheque-carburant-quelles-sont-les-pistes-7900131951)

[Prime carburant, remise à la pompe : quels dispositifs pour payer l’essence ? Quelles sont les aides en vigueur en 2022 ? - aide social.fr](https://www.aide-sociale.fr/aide-carburant/)

[Prix des carburants : quelle forme pourrait prendre la nouvelle aide envisagée par le gouvernement ? - midi libre](https://www.midilibre.fr/2022/03/08/prix-des-carburants-quelle-forme-pourrait-prendre-la-nouvelle-aide-envisagee-par-le-gouvernement-10155843.php)

[Carburant, chèque énergie, RSA et prime d’activité : ce qui change au 1er avril 2022 - lafinancepourtous](https://www.lafinancepourtous.com/2022/03/30/carburant-cheque-energie-rsa-et-prime-dactivite-ce-qui-change-au-1er-avril-2022/)

[Carburants. La remise à la pompe doublée pour les soignants libéraux - ouest france](https://www.ouest-france.fr/economie/transports/carburants-essence-diesel/carburants-la-remise-a-la-pompe-doublee-pour-les-soignants-liberaux-233fe368-c4bc-11ec-9bac-e092504fc1fc)
## Prime Ponctuelle

Sous forme **d'indemnité inflation.**

Une aide similaire, d'un montant de 100 euros, a déjà été versée entre décembre dernier et février à près de 38 millions de Français, dont le revenu est inférieur à 2.000 euros par mois. C'est une solution à court terme. Tout d'abord, elle est très couteuse, environ 3 milliards d'euros et ne peux pas être renouveler continuellement.

## aide ciblée

Aide ciblée aux professions qui souffrent le plus, parmi lesquelles les ambulanciers, taxis, marins pêcheurs ou encore agriculteurs.

Des **mesures "fiscales"** avantageuses pourraient être décidées, notamment en matière de baisse des charges, voire d’abandon du paiement de certaines d’entre elles.

## chèques carburant

un chèque carburant, sur le modèle du **ticket-restaurant**, qui serait distribué par les comités d’entreprise et permettrait de cibler les salariés qui prennent leur voiture pour travailler.

## Aide carburant de l’État : remise de 18 centimes par litre d’essence

**Pour qui ?** Tous les Français (particuliers, professionnels et entreprises) bénéficient automatiquement de la remise carburant, sans condition de revenus.

**Quels carburants ?** Tous les types de carburants, à l’exception du bio-éthanol E85.

**Pour quelle durée ?** Elle s’applique du 1er avril au 31 juillet 2022.

**Comment ça marche ?** Les prix affichés tiendront compte de la remise.

## Aide Assurance maladie: les professionnels de santé libéraux bénéficient de 0,15 € par litre de carburant d’aide supplémentaire

**Pour qui ?** "aide financière exceptionnelle" de 15 centimes par litre de carburant pour les professionnels de santé libéraux

**Pour quelle durée ?** 25 avril 2022 au 31 juillet 2022

**Comment ça marche ?**  Sous la forme d’une revalorisation des indemnités de déplacement et des indemnités kilométriques « pour atteindre une aide équivalente à 0,15 € par litre »

## Prime essence et trajets professionnels : le barème kilométrique

Fin janvier 2022, le Premier ministre Jean Castex a annoncé une hausse de 10% du barème de l’indemnité kilométrique pour l’imposition des revenus 2021.

**Pour qui ?** Vous pouvez bénéficier de ce dispositif si vous utilisez votre véhicule (voiture, scooter, moto, etc.) dans le cadre professionnel et que vous déclarez vos frais professionnels via le **barème kilométrique.** Selon le Gouvernement, cette mesure concerne **2,5 millions de Français.**

**Pour quelle durée ?** La hausse sera prise en compte via votre déclaration annuelle imposable des revenus 2021.

**Comment ça marche ?** Les personnes ayant l’habitude de déclarer leurs frais réels sur leur déclaration de revenus pourront retrancher une somme plus importante.

## Aide au carburant de la région Hauts-de-France

Avant même la forte hausse des carburants de 2022, la région des Hauts-de-France a mis en place en 2016 une aide de 20 euros par mois pour certains habitants travaillant loin de leur domicile. Il s’agit de l’Aide au Transport aux particuliers (ATP), aussi appelée « Chèque carburant ».

**Pour qui ?**

+ Les salariés du territoire qui travaillent à au moins 20 km de leur domicile et qui utilisent leur véhicule personnel dans ce cadre. L’aide est aussi soumise à des conditions de ressources : les bénéficiaires doivent avoir des revenus inférieurs à deux fois le montant du SMIC en vigueur.
+ Les étudiants du territoire qui roulent au moins 100 km par semaine avec leur véhicule particulier pour rejoindre leur lieu d’étude
+ Les familles qui roulent au moins 100 km par semaine avec leur véhicule particulier pour accompagner leur(s) enfant(s) dans les internats ou Instituts Médicaux-Educatifs (IME)

**Comment ça marche ?** Il faut faire une **demande en ligne**


## Remise station Total (juste pour le savoir)

TotalEnergies, ancien groupe Total, a décidé d’appliquer une remise de 5 euros sur les pleins de 50 litres de carburant, soit une économie de 10 centimes par litre.

**Pour qui ?** Jusqu’au 1er avril, la remise TotalEnergies concerne les personnes qui viennent faire le plein dans les stations de l’enseigne et qui vivent en zone rurale dans les communes de moins de 6.000 habitants. À compter du 1er avril, toutes les stations Total sont concernées par le remise.

**Quels carburants ?** La remise TotalEnergies s’applique aux Diesels B7 et B10, aux sans-plomb 95 et 95 E10, et aux carburants Excellium SP 95 E10, SP 98.

**Pour quelle durée ?** La remise s’applique du 14 février au 15 mai 2022.

**Comment ça marche ?** Le prix à la pompe affiche automatiquement le tarif réduit Total.

## chèque énergie 2022

**Pour qui ?** Le chèque énergie est une aide financière pour les ménages aux faibles revenus, permettant de réduire le montant des factures d’énergie (électricité, gaz, fioul, bois…).

**Comment ça marche ?**  Le chèque énergie est envoyé automatiquement chaque année, entre fin mars et fin avril. Aucune démarche n’est nécessaire, le calendrier d’envoi est fixé **par département.**

**Il existe déjà** un simulateur en ligne sur le site du ministère de la Transition écologique.