# Brève explication des différents jobs

## Build Docker image:

- Construction d'une image Docker avec c qu'il faut comme paquets R pour réaliser les appariement lors de la préparation des données.

## Build survey collections:

- Construction des fichiers HDF5 "bruts" à partir des données brutes aux formats SAS et/ou stata à partir de build-collection fournis par `openfisca-survey-manager`.
- Ces fichiers seront manipulés par des scripts python pour construire les données permettant d'initialiser les simulations. La config permettant de dire où se trouvent les données brutes est dans `runner/openfisca_survey_manager_raw_data.ini` et il faut aussi un fichier `runner/openfisca_survey_manager_config.ini` pour gérer où l'on va mettre les fichiers HDF5 les meta data etc

## Build matched data:

- C'est l'étape où l'on construit les données appariées (matched data).

`build-survey-data` pointe vers `openfisca_france_indirect_taxation.scripts.build_survey_data.main` qui lance les différents scripts de préparation de donéesLes 3 premières étapes sont déclenchées manuellement car on n'a pas besoin de les faire tourner à chaque modification du code.

## Test:
- Lance quelques tests unitaires.