# Chose à retenir:

- parc_moyen_vehicule_france.cvs:

Nous donne le nombre de vehicule en France selon certaines catégories
On la les données jusque 2020, hypothèse de constance pour 2021/2022

données INSEE: https://www.insee.fr/fr/statistiques/2045167#figure1_radio2

- parcours_moyen_vehicule_france

Nous donne le parcours moyen des vehicules en france selon certaines catégories (km)
On la les données jusque 2020, hypothèse de constance pour 2021/2022

On la les données jusque 2020, hypothèse de constance pour 2021/2022
données INSEE: https://www.insee.fr/fr/statistiques/2045167#figure1_radio2

- consommation_moyenne_carburant_france.csv:

Nous donne la consommation moyenne par type de vehicule.
On la les données jusque 2020, hypothèse de constance pour 2021/2022

données: https://fr.statista.com/statistiques/486554/consommation-de-carburant-moyenne-voiture-france/
données: https://fr.statista.com/statistiques/487208/consommation-de-carburant-moyenne-vehicule-lourd-france/

(pour gpl, consommation est considéré comme 20% superieur à celle de l'essence, source: https://www.antargaz.fr/gpl-carburant/avantages)

- part_des_types_de_supercarburants.csv

Nous donne la part de chaque supercarburant.
On la les données jusque 2021, hypothèse de constance pour 2022

données: Rapports sur l'industrie pétrolière et gazière et ufip jusque 2017 (sans la part de l'E85, modification fait par l'IPP)
données: 2018/2019 https://www.statistiques.developpement-durable.gouv.fr/sites/default/files/2020-09/datalab_essentiel_223_ventes_produits_petroliers_2019_septembre2020.pdf
données: 2020/2021 https://www.energiesetmobilites.fr/uploads/pdf/Etude_reseau_2021DEF.pdf
