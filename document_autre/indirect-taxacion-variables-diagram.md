# ticpe.py

## ticpe


```mermaid
graph LR

Z99[total_taxes_energies] --> A1[essence_ticpe]
Z99[total_taxes_energies] --> A2[diesel_ticpe]
Z99[total_taxes_energies] --> A3[combustibles_liquides_ticpe]

Z1[ticpe_totale] --> A1[essence_ticpe]
Z1[ticpe_totale] --> A2[diesel_ticpe]

A1[essence_ticpe] --> B11[sp95_ticpe]
A1[essence_ticpe] --> B12[sp98_ticpe]
A1[essence_ticpe] --> B13[sp_e10_ticpe]
A1[essence_ticpe] --> B14[super_plombe_ticpe]

B11[sp95_ticpe] --> C111[depenses_sp_95]

B12[sp98_ticpe] --> C121[depenses_sp_98]

B13[sp_e10_ticpe] --> C131[depenses_sp_e10]
B13[sp_e10_ticpe] --> C132[depenses_sp_95]

B14[super_plombe_ticpe] --> C141[depenses_sp_95]
B14[super_plombe_ticpe] --> C142[depenses_super_plombe]


A2[diesel_ticpe] --> B21[depenses_diesel]

A3[combustibles_liquides_ticpe] --> B31[quantites_combustibles_liquides]

```

## ticpe ajusté
```mermaid
graph LR

Z2[ticpe_totale_ajustee] --> A1[essence_ticpe_ajustee]
Z2[ticpe_totale_ajustee] --> A2[diesel_ticpe_ajustee]

A1[essence_ticpe_ajustee] --> B1[sp95_ticpe_ajustee]
A1[essence_ticpe_ajustee] --> B2[sp98_ticpe_ajustee]
A1[essence_ticpe_ajustee] --> B3[sp_e10_ticpe_ajustee]
A1[essence_ticpe_ajustee] --> B4[super_plombe_ticpe_ajustee]

B1[sp95_ticpe_ajustee] --> C1[depenses_essence_ajustees]

B2[sp98_ticpe_ajustee] --> C21[depenses_essence_ajustees]

B3[sp_e10_ticpe_ajustee] --> C31[depenses_essence_ajustees]

B4[super_plombe_ticpe_ajustee] --> C41[depenses_sp_95]
B4[super_plombe_ticpe_ajustee] --> C42[depenses_super_plombe]


A2[diesel_ticpe_ajustee] --> B[depenses_diesel_ajustees]


```

## difference ticpe ajusté - ticpe

```mermaid
graph LR

A1[difference_ticpe_diesel_reforme] --> B1[diesel_ticpe_ajustee]
A1[difference_ticpe_diesel_reforme] --> B2[diesel_ticpe]

```

```mermaid
graph LR

A1[difference_ticpe_diesel_reforme] --> B1[essence_ticpe_ajustee]
A1[difference_ticpe_diesel_reforme] --> B2[essence_ticpe]

```
```mermaid
graph LR

A1[difference_ticpe_totale_reforme] --> B1[ticpe_totale_ajustee]
A1[difference_ticpe_diesel_reforme] --> B2[ticpe_totale]

```

## quantité

```mermaid
graph LR

A[quantite_diesel] --> B[depenses_diesel]

```
```mermaid
graph LR

A[quantite_essence] --> B1[quantite_sp95]
A[quantite_essence] --> B2[quantite_sp98]
A[quantite_essence] --> B3[quantite_sp_e10]
A[quantite_essence] --> B4[quantite_super_plombe]


B1[quantite_sp95] --> C11[depenses_sp_95]

B2[quantite_sp98] --> C21[depenses_sp_98]

B3[quantite_sp_e10] --> C31[depenses_sp_e10]
B3[quantite_sp_e10] --> C32[depenses_sp_95]

B4[quantite_super_plombe] --> C41[depenses_sp_95]
B4[quantite_super_plombe] --> C42[depenses_super_plombe]

```



# quantites_energy.py

```mermaid
graph LR

A[quantites_combustibles_liquides] --> B[depenses_combustibles_liquides]

```

```mermaid
graph LR

A[quantites_diesel] --> B[depenses_diesel]

```

```mermaid
graph LR

A[quantites_essence] --> B1[quantites_sp95]
A[quantites_essence] --> B2[quantites_sp98]
A[quantites_essence] --> B3[quantites_sp_e10]
A[quantites_essence] --> B4[quantites_super_plombe]


B1[quantites_sp95] --> C1[depenses_essence]

B2[quantites_sp98] --> C1[depenses_sp_98]

B3[quantites_sp_e10] --> C1[depenses_essence]

B4[quantite_super_plombe] --> C1[depenses_essence]


```
