# mise en forme:

## legende

```mermaid
graph LR

A1[variable] --> B11[fichier yaml]
A1[variable] --> B12[donnée entré]

    style B12 fill:#339999
    style B11 fill:#9900cc
```


## ticpe_cas_type.py

```mermaid
graph LR


Z2[cout_total_ttc] --> Z1[total_taxes_energies]
Z2[cout_total_ttc] --> V1[Prix_total_ht]
Z2[cout_total_ttc] --> V2[tva_produit]
Z2[cout_total_ttc] --> Z3[tva_ticpe]

V2[tva_produit] --> V1[cout_total_ht]
V2[tva_produit] --> V11[taux_tva]
    style V11 fill:#9900cc

Z3[tva_ticpe] --> Z1[total_taxes_energies]
Z3[tva_ticpe] --> V11[taux_tva]
    style V11 fill:#9900cc

V1[cout_total_ht] --> W11[nombre_litre_diesel]
V1[cout_total_ht] --> W12[prix_ht_diesel]
V1[cout_total_ht] --> W13[nombre_litre_combustibles_liquides]
V1[cout_total_ht] --> W14[prix_ht_combustibles_liquides]
V1[cout_total_ht] --> W15[nombre_litre_sp95]
V1[cout_total_ht] --> W16[prix_ht_sp95]
V1[cout_total_ht] --> W17[nombre_litre_sp98]
V1[cout_total_ht] --> W18[prix_ht_sp98]
V1[cout_total_ht] --> W19[nombre_litre_sp_e10]
V1[cout_total_ht] --> W110[prix_ht_sp_e10]
V1[cout_total_ht] --> W111[nombre_litre_super_plombe]
V1[cout_total_ht] --> W112[prix_ht_super_plombe]

    style W11 fill:#339999
    style W12 fill:#9900cc
    style W13 fill:#339999
    style W14 fill:#9900cc
    style W15 fill:#339999
    style W16 fill:#9900cc
    style W17 fill:#339999
    style W18 fill:#9900cc
    style W19 fill:#339999
    style W110 fill:#9900cc
    style W111 fill:#339999
    style W112 fill:#9900cc



Z1[total_taxes_energies] --> A1[essence_ticpe]
Z1[total_taxes_energies] --> A2[diesel_ticpe]
Z1[total_taxes_energies] --> A3[combustibles_liquides_ticpe]


A1[essence_ticpe] --> B11[sp95_ticpe]
A1[essence_ticpe] --> B12[sp98_ticpe]
A1[essence_ticpe] --> B13[sp_e10_ticpe]
A1[essence_ticpe] --> B14[super_plombe_ticpe]

A2[diesel_ticpe] --> B21[nombre_litre_diesel]
A2[diesel_ticpe] --> B22[prix_ht_diesel]

    style B21 fill:#339999
    style B22 fill:#9900cc

A3[combustibles_liquides_ticpe] --> B31[nombre_litre_combustibles_liquides]
A3[combustibles_liquides_ticpe] --> B32[prix_ht_combustibles_liquides]

    style B31 fill:#339999
    style B32 fill:#9900cc

B11[sp95_ticpe] --> C111[nombre_litre_sp95]
B11[sp95_ticpe] --> C112[prix_ht_sp95]

    style C111 fill:#339999
    style C112 fill:#9900cc

B12[sp98_ticpe] --> C121[nombre_litre_sp98]
B12[sp98_ticpe] --> C122[prix_ht_sp98]

    style C121 fill:#339999
    style C122 fill:#9900cc

B13[sp_e10_ticpe] --> C131[nombre_litre_sp_e10]
B13[sp_e10_ticpe] --> C132[prix_ht_sp_e10]

    style C131 fill:#339999
    style C132 fill:#9900cc

B14[super_plombe_ticpe] --> C141[nombre_litre_super_plombe]
B14[super_plombe_ticpe] --> C142[prix_ht_super_plombe]

    style C141 fill:#339999
    style C142 fill:#9900cc

```

## questions

Pourquoi passer du TTC au HT alors qu'il y a de l'HT dans les bases de données?

## autre

prix ht general pour pouvoir donner la ticpe par region par dessus et les TVA_ticpe et TVA_produit.