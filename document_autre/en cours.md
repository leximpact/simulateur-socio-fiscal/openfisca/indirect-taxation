# question semaine du 9 mai

## Région:


`accise_diesel = parameters(period).imposition_indirecte.produits_energetiques.ticpe.gazole`
`major_regionale_ticpe_gazole = parameters(period).imposition_indirecte.produits_energetiques.major_regionale_ticpe_gazole[region]`
`majoration_ticpe_diesel = accise_diesel + major_regionale_ticpe_gazole `


Cela ne fonctionne que si Toutes les regions exites, le problème étant que toutes ne sont pas majorées, et ne sont donc pas présente dans `parameters(period).imposition_indirecte.produits_energetiques.major_regionale_ticpe_gazole`. De plus, la technique d'utiliser `np.fromiter` ne fonctionne pas quand on a plusieurs ménages, la sortie est identique pour chaque valeur du vecteur peut importe la majoration individuelle qui est diffèrente (je n'ai pas creusé plus pour savoir pourquoi).

solution envisagable:
- Rajouter une valeur **autre** dans le fichier yaml comme région avec 0 en valeur, et laisser le front-end gérer le fait que les région qui ne sont pas majorées soit considérées comme tel.
- Rajouter tous les régions dans le fichier yaml avec 0 en valeur si pas de majoration (pas très fan de l'idée).
- Trouver un code qui permet de passer outre le problème.

## ttc to ht:

- Prix ttc du chemin `parameters(period).prix_carburants.diesel_ttc` est en hectolitre, pourquoi?

- Problème du fait de passer par un prix ttc: le prix ht sera different en fonction de la population en entrée. On peut se rapprocher des chiffres de l'IPP mais pas être identique.

## script ttc:

- le script fonctionne mais manque quelques années.

- avoir le prix ht direct serait mieux.


# question semaine du 16 mai

## prix:

### ttc to ht

- Prix ttc du chemin `parameters(period).prix_carburants.diesel_ttc` est en hectolitre, pourquoi?
- Problème du fait de passer par un prix ttc: le prix ht sera different en fonction de la population en entrée. On peut se rapprocher des chiffres de l'IPP mais pas être identique

### script ht

- le script fonctionne mais manque quelques années.
- avoir le prix ht direct serait mieux
## region

- base de donnée budget des familles à région, mais la variable est suprimé...
- trouver un autre moyen, poid agrégé?

## base de donnée

- utilisation de la base pour optenir les quantitées à partir des dépenses
- viellissement de la base
- utilisation du module elasticité


# NOTE DU 20/05/2022
## regarder base de données et script:

- budget_des_familles 2005 2011 2017
- enquete_logement 2013
- enquete_transports 2008
- aliss
- erfs_fpr 2013

--> voir .gitlab-ci.yml
--> comprendre build_survey_data

## ajustement et CSV

il y a des ajustements de fait grace à des dataframes disponibles open data comme:
- comptabilité national

## Paramètre

--> voir processing.py

Pour le prix utilisé
part carburant, depsne carburant ect


--> depense_diesel_sp98