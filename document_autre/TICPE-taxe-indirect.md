# La TICPE

## Où trouver les informations

"Le bulletin officiel des douanes (BOD) est une publication de la direction générale des douanes et droits indirects destinée à éclairer les usagers, professionnels ou particuliers, sur de multiples sujets concernant leurs relations avec la douane (organisation, dédouanement, fiscalité, ...). Un bulletin officiel des douanes contient une décision administrative (DA) qui explicite un point de réglementation."

Circulaire Droits et taxes applicables aux produits énergétiques à compter du 1er janvier 2022:
https://www.fntr.fr/sites/default/files/2022-01/Energie-environnement-loi%20de%20finances_21-058.pdf

Pour tous les autres circulaires (il y en a beaucoup mais ça donne les valeurs et explique un peu où retrouver les choses dans la loi):
https://www.douane.gouv.fr/la-douane/informations/bulletins-officiels-des-douanes
avec comme mot clef: Droits et taxes applicables aux produits
Attention, pas forcement de changement dans les taux à chaque circulaire

Les taux dans le temps, avec des liens vers la lois (IPP):
https://www.ipp.eu/baremes-ipp/taxation-indirecte/produits_energetiques/ticpe/
Attention, ne prend pas en compte les majorations par région

## Historique:

**Avant 2011:** taxe int ́erieure sur les produits p ́etroliers.

**Apres 2011:** taxe int ́erieure de consommation sur lesproduits  ́energ ́etiques (TICPE)

**Attention:** depuis le 1er avril 2018, la TICPE s’applique egalement au butane et au propane.

**Avant le 1er janvier 2022:** les produits  ́energ ́etiques autres que les gaz naturels et les charbons (code des doines).

**Apres le 1er janvier 2022:** Le r ́egime d’accise est detaille dans les articles du chapitre II du titre Ier du livre III du Code des impositions sur les biens et services.

## Dans parameters

### major_regionale_ticpe_gazole.yaml

Lien 404 error not found

### major_regional_ticpe_gazole.yaml

Lien 404 error not found

### TICPE

Avant le 01-01-2022:

article 265 du code des douanes: https://www.legifrance.gouv.fr/codes/id/LEGIARTI000041505082/2021-07-01
https://www.legifrance.gouv.fr/codes/id/LEGIARTI000041505082/2021-07-01

Apres le 01-01-2022: https://www.legifrance.gouv.fr/codes/id/LEGISCTA000044603893/2022-01-01

### TGAP carburant:

article 266 quindecies du code des douanes: https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000037993315/2019-01-01

### type de carburant de https://www.prix-carburants.gouv.fr/

B7 -> Gazole
E5 -> SP98 ou SP95
LPG -> GPLc
E85 -> E85

### prix carburant

https://www.carburants.org/statistiques/evolution-prix-essence/
