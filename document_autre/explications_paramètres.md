#

## Artricle 265:


Il est affecté aux régions et à la collectivité territoriale de Corse une fraction de tarif applicable aux carburants vendus aux consommateurs finals sur leur territoire de 1,77 € par hectolitre, pour les supercarburants repris aux indices d'identification 11 et 11 ter, et de 1,15 € par hectolitre, pour le gazole repris à l'indice d'identification 22.

Cette fraction est déjà compris dans la valeur de l'article 265, mais toutes les régions ne l'utilise pas, c'est visible uniquement dans les circulaires:

https://www.douane.gouv.fr/sites/default/files/bod/src/dana/da/F2_15-047.pdf

POITOU-CHARENTES et CORSE ont dans cette exemple, une valeur de la TICPE inferieur de 1,15 € du montant de l'article 265 pour le gazole.

## Article 265 A bis:

Les conseils régionaux et l'assemblée de Corse peuvent majorer le tarif de la taxe intérieure de consommation applicable aux carburants vendus aux consommateurs finals sur leur territoire, dans la limite de 0,73 euro par hectolitre pour les supercarburants mentionnés aux indices d'identification 11 et 11 ter du tableau B du 1 de l'article 265 et de 1,35 euro par hectolitre pour le gazole mentionné à l'indice d'identification 22 du même tableau B.

C'est un ajout au montant de l'article 265, on ne peux pas savoir quelles sont les regions qui l'utilise et a quel niveau sans les circulaires

(POITOU-CHARENTES et CORSE)

## Article 265 ter:

2017-01-01:

Le Syndicat des transports d'Ile-de-France peut décider, par délibération, de majorer le tarif de la taxe intérieure de consommation applicable aux carburants vendus aux consommateurs finals sur le territoire de la région d'Ile-de-France résultant de l'application des articles 265 et 265 A bis, dans la limite de 1,02 € par hectolitre pour les supercarburants mentionnés aux indices d'identification 11 et 11 ter du tableau B du 1 de l'article 265 et de 1,89 € par hectolitre pour le gazole mentionné à l'indice d'identification 22 du même tableau B.

## Article 265 quinquies:

 1 euro par hectolitre en ce qui concerne les produits désignés ci-après destinés à être utilisés sur le territoire de la Corse

2002-01-01: 11 et 11 bis

2019-01-01: 11,11 bis et 11 ter