# taxation indirect, TICPE sur carburant

## pour le moment:

- Creation de la branche "cas-type"
- Fichier: litre_carburant_cas_type.py / ticpe_cas_type.py / cas_type_script / ticpe_cas_type.yaml (tests)
- Regler "YearlyVariable" -> "Variable"
- Regler majoration autre que Alsace

## A faire dans un futur proche:

- Variable region/departement par rapport à code postale ou nom de la commune --> code insee de la commune (autocompleteur)
- "Variable" par ans -> "Variable" par mois
- Mettre au propre les parametres de la legislation (au format openfisca-france ?)
- Completer les prix ht par mois/année (script)
- 1er etape cas type fini

## question:

- Script pour **"prix annuel carburant.csv"** et **"prix mensuel carburant.csv"** -> ou trouver les données ET comment ça fonctionne
- problème du prix par region, utilise l'insee, est ce qu'on peut utiliser prix escence station service par region https://www.prix-carburants.gouv.fr/rubrique/opendata/
- prix de ttc à ht, methode?
- "dépense" dans les formules
- Fonctionnement **ticpe ajusté** et **Reforme** (à tipce.py mais aussi dans le cadre general)
- fonctionnement du module **élasticité**
- donnée agrégé regional ou general




# note

Je pense qu'il y a 4 points à creuser.

- **Le premier point c'est sur les régions.**

Deja, le TICPE_ajusté et TIPCE_reform de ticpe.py, Chloé ne s'en sert pas. C'est plutôt une bonne nouvelle, et j'explique plus loin la raison de l'utilisation des réformes qui ne respect pas les normes OpenFisca mais qui pourrait.

Il n'y a pas de prise en compte du point de vu des régions, simplement parceque les datas qu'ils ont sont representatif au niveau français mais pas au niveau des régions ou des départements. C'est faisable de faire les distinctions au niveau des cas types sans problèmes, mais au niveau de la population, ça ne prendra pas en compte les differences régionales.

Pour le moment, comme ils testent l'impact des réformes par rapport à la part de la TICPE commune, ils partent du principe que tout le monde vient d'alsace et que tout le monde à donc la majoration. ça n'a pas d'influence pour eux, mais ça mérite d'être plus clean.

Pour avoir des données au niveau regionale et/ou departementale, il faut trouver des données:
- Soit de population representatif par regions
- Soit agrégé par région, ce qui seraient suffisant et peut être accessible en libre accès.

(regarder le code de la taxe d'habitation dans openfisca-france pour region/departement)


- **Le second point c'est sur les prix carburants**

Il faut refaire le script pour les prix TTC et HT annuel et mensuel par l'INSEE, dans tout les cas ça aidera l'IPP et c'est necessaire pour nous aussi.

Pour savoir si on a besoin de prendre les prix par région pour être plus précis, je ne suis pas sur à 100% que ce soit nécessaire, on peut très bien partir du prix moyen français peut importe la région, mais avec une taxe differente imposé en fonction de l'endroit, c'est largement suffisant et peut être plus facile pour faire des comparaisons, surtout que les différences entre région ne doivent pas être énorme. à réflechir!

Il faut vérifier si des bases de donnée directement ht n'existe pas à l'INSEE.

- **Le troisième point c'est sur l'utilisation de la base de donnée Budget des familles (2017)** qui leurs permet d'avoir les dépenses, qu'il utilisent pour trouver les quantitées.
quelques point interessant:

Ils ne vieillissent pas la base, car ils n'en ont pas l'interet, et la base peut être vieillit selon Chloé en se réaxant simplement sur le budget globale de l'année 2021 par exemple et en gardant la même disposition des ménages ect.

survey senario: semble important dans le passage de la base de donnée à la quantitée / buildsurvey data (2017)

Ils l'ont codé dans Openfisca comme c'etait juste plus rapide, mais ça n'a rien a faire dans openfisca normalement pour être propre, le passage des dépenses jusqu'aux quantitées devraient être fait en amont.

pandas est un package utile.

- **Le quatrième point est l'utilisation du module d'élasticité.**

Si ils utilisent des réformes, c'est parceque le fait de changer la TICPE, change le prix, ce qui change la quantitée du fait de la prise en compte de l'aspect comportement. Dans un premier temps on peut faire sans, mais de ma vision ça biaise forcement les résultats agrégé, et si on veut pouvoir fusionner, il faut pouvoir le prendre en compte sur du long terme. Tout semble être fait, mais pas forcement au bon endroit pour que ce soit "propre" et Chloé semble penser que c'est possible de le rendre propre.

(voir reforme enerie budgets 2018 2019 dans budgets dans projets)

(projet loi finance 2018: evaluation de la reforme sur le TICPE (evaluation préalable du plf)
 --> Interessant de regarder les hyphothèses qu'ils ont pris dans leurs évaluations)

Il font l'hypothése que l'élasticité prix est identique pour tout les individus Français.

- **autre.**

PROMETEOUS

# Pour finir

Ce qui a déjà été fait est très bien, ce qu'il manque c'est le travail en amont, avec la variable Région et comment on la prend en compte correctement. La variable quantité, qui doit être pouvoir rentré à la mains, mais qui doit pouvoir aussi venir des datas disponible par 'depense' comme l'IPP, et/ou par d'autre sources. Les prix TTC qui passe en HT avant de rentrer dans le moteur de calcul. ect