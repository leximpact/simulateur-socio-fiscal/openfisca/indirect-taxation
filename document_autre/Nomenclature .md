# TICPE

## Avant le 01-01-2022:

article 265 du code des douanes: https://www.legifrance.gouv.fr/codes/id/LEGIARTI000041505082/2021-07-01 https://www.legifrance.gouv.fr/codes/id/LEGIARTI000041505082/2021-07-01

```mermaid
graph LR

A1{diesel} -->|de 2017-01-01 à présent| B11[gazole B7]
A1{diesel} --> B12[gazole B10]

B11[gazole B7] --> C111[22]
B12[gazole B10] --> C121[22 bis]

    style B11 fill:#9900cc
    style B12 fill:#9900cc
```

```mermaid
graph LR

A2{essence} --> B21[SP95 - E5]
A2{essence} --> B22[SP98 - E5]
A2{essence} --> B23[SP95 - E10]
A2{essence} --> B24[E85]
A2{essence} --> B25[Super Plombé]

B21[SP95 - E5] --> C211[11]
B22[SP98 - E5] --> C221[11]
B23[SP95 - E10] --> C231[11 ter]
B24[E85] --> C241[55]
B25[Super Plombé] -->C251[11 bis]

    style B21 fill:#9900cc
    style B22 fill:#9900cc
    style B23 fill:#9900cc
    style B24 fill:#9900cc
    style B25 fill:#9900cc
```

```mermaid
graph LR

A3{gaz} --> B31[GPLc]
B31[GPLc] --> C311[30 ter, 31 ter, 34]

    style B31 fill:#9900cc

```

```mermaid
graph LR

A1{type} --> B11[nom]
B11[nom] --> C111[nomenclature]

    style B11 fill:#9900cc
```

## Apres le 01-01-2022:

https://www.legifrance.gouv.fr/codes/id/LEGISCTA000044603893/2022-01-01