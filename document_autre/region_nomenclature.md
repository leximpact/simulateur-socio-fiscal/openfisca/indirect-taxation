# Tableaux Régions

## Métropole

| Nouvelles Régions         | Anciennes Régions fusionnées |
| :---------------          | -----:|
| Auvergne-Rhône-Alpes (84)      | Auvergne (83) |
|                           | Rhône-Alpes (82) |
| Bourgogne-Franche-Comté (27)   | Bourgogne (26) |
|                           | Franche-Comté (43) |
| **Bretagne (53)**                 | **Bretagne (53)** |
| **Centre-Val de Loire (24)**      | **Centre (24)** |
| **Corse (94)**                    | **Corse (94)** |
| Grand Est (44)                 | 	Alsace (42) |
|                           | Champagne-Ardenne (21) |
|                           | Lorraine (41) |
| Hauts-de-France (32)           | Nord-Pas-de-Calais (31) |
|                           | Picardie (22) |
| **Île-de-France (11)**            | **Île-de-France (11)** |
| Normandie (28)                 |	Basse-Normandie (25) |
|                           | Haute-Normandie (23) |
| Nouvelle-Aquitaine (75)        | Aquitaine (72) |
|                           | Limousin (74) |
|                           | Poitou-Charentes (54) |
| Occitanie (76)                 |	Languedoc-Roussillon (91) |
|                           | Midi-Pyrénées (73) |
| **Pays de la Loire (52)**          | **Pays de la Loire (52)** |
| **Provence-Alpes-Côte d'Azur (93)** | **Provence-Alpes-Côte d'Azur (93)** |

## Outre-Mer

| Nouvelles Régions | Anciennes Régions fusionnées |
| :---------------  | -----:|
| Guadeloupe (01) | Guadeloupe (01) |
| Martinique (02) | Martinique (02) |
| Guyane (03) | Guyane (03) |
| La Réunion (04) | La Réunion (04) |
| Mayotte (06) | Mayotte (06) |

# carte représentative

![](https://www.mieux-vivre-autrement.com/wp-content/uploads/2016/01/avant-apres.jpg)