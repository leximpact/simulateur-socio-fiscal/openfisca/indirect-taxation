# Openfisca-france-indirect-taxation (parameters)

## legend

```mermaid
graph LR

    Z1{section} --> A1[subsection]
    A1 --> B1[yaml]
    B1 --> C1(inside of the yaml)

    style B1 fill:#9900cc
    style C1 fill:#339999

```

## Imposition indirect

```mermaid
graph LR
    Z1{imposition_indirect} --> A11[produits_emergetiques]
    Z1 --> A13[taxes_energie_dans_logement]
    Z1 --> A14[tva]
    Z1 --> A15[taxes_tabacs]
    Z1 --> A16[taxes_assurances]
    Z1 --> A17[taxes_alcools_autres_boissons]


    A11 --> B1[major_rehionale_ticpe_gazole]
    B1 --> C1(Regional surcharge - diesel - par region)
    style C1 fill:#339999

    A11 --> B2[major_regionale_ticpe_gazole]
    B2 --> C2(Regional surcharge - gasoline - par region)
    style C2 fill:#339999

    A11 --> B3[tgap_carburants]
    B3 --> C31(super 95)
    B3 --> C32(Super 98)
    B3 --> C33(Super avec additifs - ARS)
    B3 --> C34(Super E10)
    B3 --> C35(Super E85)
    B3 --> C36(Gazole routier)
    B3 --> C37(Gazole B30)
    style C31 fill:#339999
    style C32 fill:#339999
    style C33 fill:#339999
    style C34 fill:#339999
    style C35 fill:#339999
    style C36 fill:#339999
    style C37 fill:#339999


    A11 -->|après 2002-01-01: currency-EUR| B4[ticpe]
    B4 --> C41(Goudrons utilisés comme combustibles - 100kg)
    B4 --> C42(White Spirit utilisé comme combustible - hectolitre)
    B4 --> C43(Essences spéciales utilisées comme carburants et combustibles - hectolitre)
    B4 --> C44(Huiles légères et préparation, essence d'aviation - hectolitre)
    B4 --> C45(Supercarburants dont SP95 et SP98 - hectolitre*)
    B4 --> C46(Supercarburants contenant un additif - hectolitre)
    B4 --> C47(SuperE10 -hectolitre*)
    B4 --> C48(Essence normale - hectolitre)
    B4 --> C49(Carburateurs type essence, sous conditions)
    B4 --> C410(Carburateurs type essence, carburants pour avion)
    B4 --> C411(Carburateurs type essence, autres)
    B4 --> C412(Autres huiles légères)
    B4 --> C413(Pétrole lampant utilisé comme combustible)
    B4 --> C414(Pétrole lampant, autre)
    B4 --> C415(Carburéacteurs, type pétrole lampant , sous conditions)
    B4 --> C416(Carburéacteurs, type pétrole lampant, carburant moteurs d'avion)
    B4 --> C417(Carburéacteurs, type pétrole lampant, autres)
    B4 --> C418(Autres huiles moyennes)
    B4 --> C419(Gazole comme carburants sous conditions)
    B4 --> C421(Gazole, fioul domestique)
    B4 --> C422(Gazole, autres*)
    B4 --> C423(Gazol B 10)
    B4 --> C424(Fioul lourd)
    B4 --> C425(Fioul avec point d'éclair inférieur à 120 C)
    B4 --> C426(Fioul lourd HTS - 100kg)
    B4 --> C427(Fioul lourd BTS - 100kg)
    B4 --> C428(Propane utilisé comme carburants, sous conditions - 100kg nets)
    B4 --> C429(Propane utilisé comme carburants, autres - 100kg nets)
    B4 --> C431(Propane utilisé comme carburants, usages autres que comme carburant - 100kg nets)
    B4 --> C432(Butanes liquéfiés, sous condition - 100kg nets)
    B4 --> C433(Butanes liquéfiés, autres)
    B4 --> C434(Butanes liquéfiés, usages autres que comme carburant)
    B4 --> C435(Autres gaz de pétrole liquéfiés utilisés comme carburants, sous conditions)
    B4 --> C436(Autres gaz de pétrole liquéfiés utilisés comme carburants, autres)
    B4 --> C437(Gaz naturel à l'état gazeux, utilisés comme carburants - m3)
    B4 --> C438(Gaz naturel à l'état gazeux, utilisé sous conditions aux moteurs stationnaires)
    B4 --> C439(Emulsion d'eau dans du gazole sous conditions - hectolitre)
    B4 --> C441(Emulsion d'eau dans du gazole autres - hectolitre)
    B4 --> C442(Super E 85 utilisé comme carburant)
    B4 --> C443(Carburant constitué d'au minimum 90% d'alcool éthylique agricole)
    B4 --> C444(Carburant constitué à 100% d'estars méthyliques d'acides gras)
    style C41 fill:#339999
    style C42 fill:#339999
    style C43 fill:#339999
    style C44 fill:#339999
    style C45 fill:#339999
    style C46 fill:#339999
    style C47 fill:#339999
    style C48 fill:#339999
    style C49 fill:#339999
    style C410 fill:#339999
    style C411 fill:#339999
    style C412 fill:#339999
    style C413 fill:#339999
    style C414 fill:#339999
    style C415 fill:#339999
    style C416 fill:#339999
    style C417 fill:#339999
    style C418 fill:#339999
    style C419 fill:#339999
    style C412 fill:#339999
    style C421 fill:#339999
    style C422 fill:#339999
    style C423 fill:#339999
    style C424 fill:#339999
    style C425 fill:#339999
    style C426 fill:#339999
    style C427 fill:#339999
    style C428 fill:#339999
    style C429 fill:#339999
    style C431 fill:#339999
    style C432 fill:#339999
    style C433 fill:#339999
    style C434 fill:#339999
    style C435 fill:#339999
    style C436 fill:#339999
    style C437 fill:#339999
    style C438 fill:#339999
    style C439 fill:#339999
    style C441 fill:#339999
    style C442 fill:#339999
    style C443 fill:#339999
    style C444 fill:#339999



    A13 --> B5[taxes_electricite_et_gaz];
    B5 --> C151(CSPE);
    B5 --> C152(CTA portant sur la distribution d'électricité);
    B5 --> C153(CTA portant sur la distribution de gaz);
    B5 --> C154(TICGN - prix par MWh);
    B5 --> C155(CTSSG - prix par MWh);
    B5 --> C156(Plafonds TCFE pour compteurs < ou = à 36 kva - en euros par MWh);

    style C151 fill:#339999
    style C152 fill:#339999
    style C153 fill:#339999
    style C154 fill:#339999
    style C155 fill:#339999
    style C156 fill:#339999

    A14 --> B6[taux_de_tva]
    B6 --> C161(Taux réduit)
    B6 --> C162(Taux intermédiaire)
    B6 --> C163(Taux normal)
    B6 --> C164(Taux majoré)
    B6 --> C165(Taux particulier - super réduit)
    B6 --> C166(Taux réduit 2)
    B6 --> C167(Taux normal ou majoré temporaire)

    style C161 fill:#339999
    style C162 fill:#339999
    style C163 fill:#339999
    style C164 fill:#339999
    style C165 fill:#339999
    style C166 fill:#339999
    style C167 fill:#339999


    A15 --> B7[prix_tabac]
    A15 --> B8[taux_normaux_tabac]
    A15 --> B9[taux_specifique_tabac]


    B7 --> C71(nan)
    B8 --> C81(nan)
    B9 --> C91(nan)

    style C71 fill:#339999
    style C81 fill:#339999
    style C91 fill:#339999

    A16 --> B10[fgao]
    A16 --> B11[tsca]

    B10 --> C101(nan)
    B11 --> C111(nan)

    style C101 fill:#339999
    style C111 fill:#339999



    A17 --> B12[alcool_cotisation_secu]
    A17 --> B13[alcool_produits_intermediaires]
    A17 --> B14[alcools_fermentes]
    A17 --> B15[bieres]
    A17 --> B16[boissons_sucrees_energisantes]
    A17 --> B17[contrib_sur_boissons_sucrees]
    A17 --> B18[boissons_non_alcooliques]
    A17 --> B19[premix_contribution_secu]


    B12 --> C121(nan)
    B13 --> C131(nan)
    B14 --> C141(nan)
    B15 --> C1151(nan)
    B16 --> C1161(nan)
    B17 --> C171(nan)
    B18 --> C1181(nan)
    B19 --> C1191(nan)


    style C121 fill:#339999
    style C131 fill:#339999
    style C141 fill:#339999
    style C1151 fill:#339999
    style C1161 fill:#339999
    style C171 fill:#339999
    style C1181 fill:#339999
    style C1191 fill:#339999






    style B1 fill:#9900cc
    style B2 fill:#9900cc
    style B3 fill:#9900cc
    style B4 fill:#9900cc
    style B5 fill:#9900cc
    style B6 fill:#9900cc
    style B7 fill:#9900cc
    style B8 fill:#9900cc
    style B9 fill:#9900cc
    style B10 fill:#9900cc
    style B11 fill:#9900cc
    style B12 fill:#9900cc
    style B13 fill:#9900cc
    style B14 fill:#9900cc
    style B15 fill:#9900cc
    style B16 fill:#9900cc
    style B17 fill:#9900cc
    style B18 fill:#9900cc
    style B19 fill:#9900cc




```
## prestations

```mermaid
graph LR;

    Z1{prestations} --> A11[cheque_energie]

    A11--> B111(Cas des ménages avec 1 UC);
    A11 --> B112(Cas des ménages avec entre 1 et 2 UC);
    A11 --> B113(Cas des ménages avec 2 UC ou plus)

    style B111 fill:#339999
    style B112 fill:#339999
    style B113 fill:#339999

    style A11 fill:#9900cc


```

## tarifs_energie

```mermaid
graph LR;

    Z1{tarifs_energie} --> A11[tarifs_reglementes_edf]
    Z1 --> A12[tarifs_reglementes_gdf]
    Z1 --> A13[prix_fioul_domestique]

    A11 --> B111(prix_unitaire_base_edf_ht)
    A11 --> B112(prix_unitaire_base_edf_ttc)
    A11 --> B113(tarif_fixe_base_edf_ttc)
    A11 --> B114(tarif_fixe_base_edf_ttc)

    A12 --> B121(prix_unitaire_gdf_ttc)
    A12 --> B122(prix_unitaire_gdf_par_zone_ht)
    A12 --> B123(tarif_fixe_gdf_ht)
    A12 --> B124(tarif_fixe_gdf_ttc)


    style B111 fill:#9900cc
    style B112 fill:#9900cc
    style B113 fill:#9900cc
    style B114 fill:#9900cc
    style B121 fill:#9900cc
    style B122 fill:#9900cc
    style B123 fill:#9900cc
    style B124 fill:#9900cc
    style A13 fill:#9900cc

    B111 --> C1111(nan)
    B112 --> C1121(nan)
    B113 --> C1131(nan)
    B114 --> C1141(nan)

    B121 --> C1211(nan)
    B122 --> C1221(nan)
    B123 --> C1231(nan)
    B124 --> C1241(nan)

    style C1111 fill:#339999
    style C1121 fill:#339999
    style C1131 fill:#339999
    style C1141 fill:#339999

    style C1211 fill:#339999
    style C1221 fill:#339999
    style C1231 fill:#339999
    style C1241 fill:#339999




```