liste des notebooks et ordre:

1 - creation_csv_donnee_insee.ipynb

-> Permet de recupèrer  Nombre de vehicule en France, Distance moyenne en km en France et Consommation moyenne de carburant par 100km en France 
-> Quelques graphes et pourcentages interessant 
-> Créer des dataframes dans le dossier "donnees_INSEE_finales" qui seront utilisées par la suite.

2 - creation_csv_bdf_final.ipynb

-> Import BDF 2017, récupère les variables utiles, modifie certaines
-> calage des 3 vecteurs vehicules sur 2020 (données insee)
-> calage dépenses sur 2021 (sur la comptabilité nationale)
-> calage revenue disponible (au sens de l'ERFS) par decile de niveau de vie + quelques graphes
-> Création des variables elasticités
-> Création d'une dataframe dans le fichier "donnees_bdf_finales" qui sera utilisée plus tard

3 - reforme_ticpe 

(tentative d'avoir une sortie globale avec tous les types de carburant avant et après reforme avec elasticité. Malheuresement mis de coté par manque de temps, j'ai donc fait l'etape 4, qui est la même chose mais pour un seul carburant)

4 - reforme_ticpe_gazole ( en cours)

-> import dataframe du dossier "donnees_bdf_finales"
-> fait toutes les étapes d'une reforme avec elasticité
-> sort 2 CSV avant/apres 

5 - reforme_graphique

(etape final pour sortir des graphes de la dataframe sortie en etape 3 et/ou 4. Malheuresement manque de temps)



0 - explorations BDF

-> des graphes et des states sur BDF 2017, qui avait été fait en phase exploratoire